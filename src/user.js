// User data model

class User  {
    constructor(name, email) {
        this.id = auth_user_id();

        if(!name && (name.length < 1 || name.length > 150)) {
            throw new RangeError('Invalid name')
        }
        this.name = name;
       
        if(!email && (email.length < 3 && email.length > 320)) {
            throw new RangeError('Invalid email address')
        }
        this.email = email;
        
        this.role = "user";

        Object.freeze(this)
    }
}

// A psudo authenticator 
// returns authenticated user ID
function auth_user_id() {
    return 2;
}


module.exports = { User, auth_user_id };
